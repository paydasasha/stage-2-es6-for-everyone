import {
  controls
} from '../../constants/controls';
import {
  fighters
} from '../helpers/mockData';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', (event) => {
      if (event.code === controls.PlayerOneAttack) {
        const damage = getDamage(firstFighter, secondFighter);
        secondFighter.health -= damage;
      } else if (event.code === controls.PlayerTwoAttack) {
        const damage = getDamage(secondFighter, firstFighter);
        firstFighter.health -= damage;
      } else if (event.code === controls.PlayerOneBlock) {
        firstFighter.isBlock = true;
      } else if (event.code === controls.PlayerTwoBlock) {
        secondFighter.isBlock = true;
      }
      recalculateFighterHealth(firstFighter, secondFighter);
      if (firstFighter.health <= 0) {
        resolve(secondFighter)
      }
      if (secondFighter.health <= 0) {
        resolve(firstFighter)
      }
    })
    document.addEventListener('keyup', (event) =>{
      if (event.code === controls.PlayerOneBlock) {
        firstFighter.isBlock = false;
      } else if (event.code === controls.PlayerTwoBlock) {
        secondFighter.isBlock = false;
      }
    })
  });
}

function recalculateFighterHealth(firstFighter, secondFighter) {
  const secondFighterIndicator = document.getElementById('right-fighter-indicator');
  const firstFighterIndicator = document.getElementById('left-fighter-indicator');
  const firstFighterPercentage = firstFighter.health / firstFighter.maxHealth * 100;
  const secondFighterPercentage = secondFighter.health / secondFighter.maxHealth * 100;
  secondFighterIndicator.style.width = secondFighterPercentage + '%';
  firstFighterIndicator.style.width = firstFighterPercentage + '%';


}

export function getDamage(attacker, defender) {
  // return damage
  const blockPower = getBlockPower(defender);
  const attackPower = getHitPower(attacker);

  const damage = attackPower - blockPower;
  if (damage < 0) {
    return 0;
  }
  return damage;

}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  if (!fighter.isBlock) {
    return 0;
  }
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}