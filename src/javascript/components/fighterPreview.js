import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)

  const img = createFighterImage(fighter);
  if(positionClassName === 'fighter-preview___right'){
    img.classList.add('fighter-preview___right_img')
  }

  const fighterName = createFighterName(fighter);
  const fighterInfo = createFighterInfo(fighter);
  
  fighterElement.append(fighterName);
  fighterElement.append(fighterInfo);
  fighterElement.append(img);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterName(fighter) {
  
  const nameElement = createElement({
    tagName: 'span',
    className: 'preview___fighter-name',
  });
 nameElement.innerText = fighter.name;
  return nameElement;
}

function createFighterInfo(fighter) {
  
  const infoElement = createElement({
    tagName: 'span',
    className: 'preview___fighter-name preview___fighter-health',
  });
  infoElement.innerText = `hp:  ${fighter.health}\n attack:  ${fighter.attack}\n defense:  ${fighter.defense}`;
  return infoElement;
}